﻿using Roulette.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Repositories
{
    public interface IBetRepository
    {
        Task<Bets> placeBet(Bets bet);
        List<Bets> getBets(string id);
        List<Bets> showPreviousSpins(Bets bet);
    }
}
