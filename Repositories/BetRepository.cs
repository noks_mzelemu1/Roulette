﻿using Microsoft.Data.Sqlite;
using Roulette.Helpers;
using Roulette.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly RouletteContext db;

        public BetRepository(RouletteContext db)
        { this.db = db; }
        public async Task<Bets> placeBet(Bets bet)
        {
            await db.Bets.AddAsync(bet);
            return bet;
        }

        public List<Bets> getBets(string id)
        {
            return db.Bets.Where(x=> x.Id == id).ToList();

        }

        public List<Bets> showPreviousSpins(Bets bet)
        {
            return db.Bets.ToList();

        }
        
    }
}
