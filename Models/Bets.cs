﻿using Roulette.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Roulette.Models
{
    
    public class Bets
    {
        public string Id { get; set; }
        [Range(0, 36, ErrorMessage = "numbers must be between 0 and 36")]
        public int Number { get; set; }
        public string Color { get; set; }
        [Required(ErrorMessage = "Value is required")]
        public decimal Value { get; set; }
        [Required(ErrorMessage = "rouletteId is required")]
        public decimal EarnedValue { get; set; }

        public bool IsValid
        {
            get
            {
                return (IsColorValid(Color) || Number > 0);
            }
        }
        private static bool IsColorValid(string color)
        {
            switch (color)
            {
                case "red":
                case "black":
                    return true;
                default:
                    return false;
            }
        }
    }
}
