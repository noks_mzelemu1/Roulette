﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Roulette.Models;
using Roulette.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IBetRepository _repository;
        private readonly ILogger<BetController> _logger;
        public BetController(IBetRepository repository, ILogger<BetController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpPost]
        [Route("placeBet")]
        public async Task<ActionResult<Bets>> placeBet(Bets bet)
        {
            try
            {
                _logger.LogInformation("Place Bet");
                return await _repository.placeBet(bet);
            }
            catch (Exception ex)
            {
                _logger.LogError("", ex.Message);
            }
            return null;
        }

        [HttpGet]
        [Route("Payout")]
        public async Task<decimal> Payout(string id)
        {
            int numberWinner = new Random().Next(1, 36);
            string colorWinner = CalculateColorWinner(numberWinner);
            try
            {
                _logger.LogInformation("Get Payout By ID");
                var bets = _repository.getBets(id).ToList();

                foreach (Bets bet in bets)
                {
                    if (bet.Number == numberWinner)
                    {
                        bet.EarnedValue = bet.Value * 5;
                    }
                    else if (string.Equals(colorWinner, bet.Color))
                    {
                        bet.EarnedValue = bet.Value * 1.8m;
                    }
                    return bet.EarnedValue;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("", ex.Message);
            }

            return 0;
        }

        [HttpGet]
        [Route("showPreviousSpins")]
        public ActionResult<List<Bets>> showPreviousSpins(Bets bet)
        {
            try 
            {
                _logger.LogInformation("Show Previous Spins");
                return _repository.showPreviousSpins(bet).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError("", ex.Message);
            }
            return null;
        }
        private string CalculateColorWinner(int number)
        {
            if (number % 2 == 0)
                return "red";
            else
                return "Black";
        }

    }
}
